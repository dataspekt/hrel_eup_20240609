# Izbori članova u Europski parlament (2024-06-09)

## Sadržaj:

* Karta Hrvatske na LAU2 razini: `data/karta`

* Podaci o rezultatima prethodnih izbora: `data/prethodni_izbori`.

* Funkcije za učitavanje podataka iz JSON datoteka, D'Hondt izračun i drugo: `fun.R`: 

## Napomene

* Ovo je neslužbeni izvor podataka, nema ikakvih jamstava. *Use at your own peril!*
